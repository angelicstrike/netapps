import code
import time
import threading
import socket
import requests
import argparse

TWITCH_URL = 'https://api.twitch.tv/kraken/'
#TWITCH_URL = 'http://localhost:8888/'

#remember to set mime type and api version number
#all data is sent and received as json

def authenticate( client_id ):
    global TWITCH_URL
    auth_url = TWITCH_URL + 'oauth2/authorize'
    params = { 'client_id' : client_id, 'redirect_uri' : 'http://localhost' }
    params['scope'] = 'user_read+channel_read+channel_editor+chat_login+channel_commercial'
    params['response_type'] = 'token'
    params['force_verify'] = 'true'
    #r = requests.get( auth_url, params=params )
    r = requests.Request( "GET", TWITCH_URL, params=params )
    prepped = r.prepare()
    print prepped.url

def open_file( file_name ):
    try:
        fd = open( file_name, 'r' )
    except IOError:
        print "Could not open " + file_name + " for reading"
        exit(1)

    try:
        data = fd.read().strip()
    except IOError:
        print "Could not read in " + file_name + "'s data"
        exit(2)

    fd.close()

    return data

def grab_client_info( id_file, secret_file, access_file ):
    return open_file( id_file ), open_file( secret_file ), open_file( access_file )

def recvall( sock ):
    data = ''
    tmp = ''
    while data.endswith("\r\n") != True:
        tmp = sock.recv(1024)
        data += tmp
    return data



#all return messages have to end in \r\n
#returns 0 for do nothin, 1 for up, 2 for down, 3 for left, 4 for right
def handle_msg( sock, data ):
#msg.split implicitly contains the ending \r\n
#we really only care about two messages, PINGs to keep the connection alive, and command messages
#PART messages and other things like them are ignored, as they serve no purpose for this project
    if data.startswith( "PING " ):
        sock.send( "PONG " + data.split(" ")[0] )
        return 0
    else:
        tmp = data[ data.index("#"): ]
        msgText = tmp[tmp.index(":")+1:]
        if msgText[0:2].lower() == "up":
            return 1
        elif msgText[0:4].lower() == "down":
            return 2
        elif msgText[0:4].lower() == "left":
            return 3
        elif msgText[0:5].lower() == "right":
            return 4
        else:
            return 0

#basic message reception:
#:twitch_username!twitch_username@twitch_username.tmi.twitch.tv PRIVMSG #channel :message here

def msg_chan( sock, username, msg ):
    sock.send( "PRIVMSG #" + username + ":" + msg + "\r\n" )

#in case of ties, the order of precedence is up, down, left, right, from most important to least
def send_motor_cmd( up, down, left, right, username, sock ):
    ans = ""
    ansVert = ""
    ansHorti = ""
    if up >= down:
        ansVert = "up"
    else:
        ansVert = "down"
    
    if left >= right:
        ansHori = "left"
    else:
        ansHort = "right"

    msg = "Rover is going "
    if ansVert == "up" and ansHori == "left":
        if up >= left:
            ans += "up"
        else:
            ans += "left"
    elif ansVert == "down" and ansHori == "left":
        if down >= left:
            ans += "down"
        else:
            ans += "left"
    elif ansVert == "up" and ansHori == "right":
        if up >= right:
            ans += "up"
        else:
            ans += "right"
    elif ansVert == "down" and ansHori == "right":
        if down >= right:
            ans += "down"
        else:
            ans += "right"

    msg_chan( sock, username, msg + ans )
#mechanism to send to the rover process that's doing everything?


def loop_eternal( username, sock ):
    right = 0
    left = 0
    down = 0
    up = 0
    startTime = time.time()
    while True:
        data = recvall( sock )
        print "===================================================================================="
        print data
        direction = handle_msg( socket, data )
        if direction == 1:
            up = up + 1
        elif direction == 2:
            down = down + 1
        elif direction == 3:
            left = left + 1
        elif direction == 4:
            right = right + 1
        now = time.time()
        if( now - start > 10.0 ):
#if we haven't gotten anything, don't do anything
#maybe tell the rover to stay put?
#Why reduce? BECAUSE I CAN AHAHAHAHAHAHAH
            if reduce( lambda x,y: x + y, [up, down, left,right] ) != 0 :
                send_motor_cmd( up, down, left, right, username, sock )
            start = now
        

#twitch chat api is essentially IRC with a few differences
def run_chat( auth_token, username ):
    base_chat_url = 'irc.twitch.tv'
    chat_port = 6667
    s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
    s.connect( (base_chat_url,chat_port) )

    s.send( "PASS oauth:" + auth_token + "\r\n" )
    s.send( "NICK " + username + "\r\n" )
    #s.send( "USER " + username + " " + "localhost roverbot roverbot\r\n" )

# grab the welcome message
    data = recvall( s )
    print data
# join the channel
    s.send( "JOIN #" + username + "\r\n" )
    data = recvall( s )
    #s.send( "PRIVMSG #" + username + " :Roverbot joining the channel\r\n" )
    msg_chan( s, username, "Roverbot joining the channel" )

    loop_eternal( username, s )

#example welcome message:
#':tmi.twitch.tv 001 aegisstrike :Welcome, GLHF!\r\n:tmi.twitch.tv 002 aegisstrike :Your host is
#tmi.twitch.tv\r\n:tmi.twitch.tv 003 aegisstrike :This server is rather new\r\n:tmi.twitch.tv 004
#aegisstrike :-\r\n:tmi.twitch.tv 375 aegisstrike :-\r\n:tmi.twitch.tv 372 aegisstrike :You are in a
#maze of twisty passages, all alike.\r\n:tmi.twitch.tv 376 aegisstrike :>\r\n'

def main():
    parser = argparse.ArgumentParser( description='Twitch plays rover -- chat side' )
    parser.add_argument( '--username', '-u', help='username to broadcast from', nargs=1 )
    parser.add_argument( '--client-id', '-i', help='twitch client id', nargs=1 )
    parser.add_argument( '--client-secret', '-s', help='twitch client secret', nargs=1 )
    parser.add_argument( '--access-token', '-a', help='twitch access token', nargs=1 )

    args = parser.parse_args()

    i, s, a = grab_client_info( args.client_id[0], args.client_secret[0], args.access_token[0] )

    #authenticate( i )
    run_chat( a, args.username[0] )


if __name__ == '__main__':
    main()

 #! /usr/bin/python
import RPi.GPIO as GPIO
import threading
import signal
import socket
import errno
import time
import sys
import os

DATA_LEN = 1024
LED = 12
e = threading.Event()
k = threading.Event()


def flash( e ):
    while 1:
        if( not e.isSet() ):
            GPIO.output( LED, GPIO.HIGH )
            time.sleep(1)
            GPIO.output( LED, GPIO.LOW )
            time.sleep(1)
        if( k.isSet() ) :
            break

t = threading.Thread( name='flashmob', target=flash, args=(e,) )
def parseMsg( conn, msg ):
    if( msg == "LEDON" ):
        e.set()
        GPIO.output( LED, GPIO.HIGH )
        conn.send("ACK")
    elif( msg == "LEDFLASH" ):
        e.clear()
        conn.send("ACK")
    elif( msg == "LEDOFF" ):
        e.set()
        GPIO.output( LED, GPIO.LOW )
        conn.send("ACK")
    else:
        conn.send("NCK")

def handleClient( conn ):
    data = conn.recv( DATA_LEN )
    print "Data received: " + data
    parseMsg( conn, data.strip('\n') )


def startServer( port ):
    try:
        sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
    except socket.error as err:
        print "Could not create listening socket socket"
        print "Error: " + os.strerror( err.errno )
        return 
    try:
        sock.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
        sock.bind( ('', port) ) #host and port
        sock.listen(1) #backlog length
    except socket.error as err:
        print "Error editing the socket"
        print "Error: " + os.strerror( err.errno )
        sock.close()
        return
    t.start()

    while True:
        conn, addr = sock.accept()
        print "Received connection from ", addr
        handleClient( conn )
        conn.close()
        print "Closed connection: ", addr

def signal_handler(signal, frame):
    GPIO.cleanup() 
    k.set()
    t.join()
    sys.exit(0)
    print "Exiting..."

if( __name__ == '__main__' ):
    if len(sys.argv) != 2:
        print "Please enter in a valid port number to use (and only a port number)"
        exit(1)
    try:
        portNum = int( sys.argv[1] )
    except ValueError:
        print "Invaid number entered for the port"
        exit(2)
    signal.signal(signal.SIGINT, signal_handler)
    GPIO.setmode( GPIO.BOARD )
    GPIO.setup( LED, GPIO.OUT )
    GPIO.output( LED, GPIO.LOW )
    e.set()
    startServer( portNum )

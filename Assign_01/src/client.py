#! /usr/bin/python
import socket
import getopt
import tweepy
import errno
import time
import json
import sys
import os
import re

DATA_LEN = 1024
def sendToServer( host, port, msg, api ):
    try:
        sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
    except socket.error as err:
        print "Error creating socket"
        print "Error: " + os.strerror( err.errno )
        return

    try:
        sock.connect( (host, port) )
    except socket.error as err:
        sock.close()
        print "Error connecting to remote host"
        print "Error: " + os.strerror( err.errno )
        return
    print "Transmitting instruction: " + msg
    sock.send( msg )
    data = sock.recv(DATA_LEN)
    print "Received Message: " + data.strip('\n')
    if( data.strip('\n') == "ACK" ):
        api.update_status(status="@VTNetApps ACK " + time.ctime() + " " + '#ECE4564_' + host.replace('.', '_') + '_'+str(port)+'_'+msg )

    sock.close()

class StdOutListener(tweepy.StreamListener):
    def __init__(self, twitterAPI):
        self.api = twitterAPI

    def parseHashTag(self, hashTag):
        msg = hashTag.split('_')
        try:
            ip = '.'.join(msg[1:5])
            port = msg[5]
            cmd = msg[6]
        except:
            print "invalid hashtag"
            return None, None, None
        return ip, port, cmd
    
    def checkHashTag(self, hashTags):
        if( len(hashTags) != 1 ):
            return False
        hashTag = hashTags[0]['text']
        m = re.search('ECE4564\_([1-9]{1,3}\_){4}[1-6]?[0-9]{1,4}\_LED[ON,OFF,FLASH]', hashTag)
        if( m == None ):
            return False

        return True
    def on_data(self, status):
        json_data = json.loads(status)
        #do something with json_data
        if( 'user' not in json_data ):
            return True
        if( self.api.me().screen_name == json_data['user']['screen_name'] ):
            return True
        if( 'entities' not in json_data ):
            return True
        if( 'hashtags' in json_data['entities'] ):
            if( self.checkHashTag( json_data['entities']['hashtags']) ):
                host, port, cmd = self.parseHashTag(json_data['entities']['hashtags'][0]['text'])
                print "Received hashtag: " + json_data['entities']['hashtags'][0]['text']
                if( host == None or port == None or cmd == None ):
                    return True
                sendToServer( host, int(port), cmd, self.api )
        return True

    def on_exception(self, exception):
        print "Exception: " + str(exception)
        return True
  
    def on_error(self, status_code):
        print('Got an error with status code: ' + str(status_code))
        return True # To continue listening
 
    def on_timeout(self):
        print('Timeout...')
        return True # To continue listening
 

def authenticate( keyFileName, secretFileName, accessKeyFile, accessSecretFile ):
    try:
        cKeyHandle = open( keyFileName, "r" )
        cSecretHandle = open( secretFileName, "r" )
    except IOError:
        print "Could not open consumer file(s) for reading"
        exit(1)

    try:
        aKeyHandle = open( accessKeyFile, "r" )
        aSecretHandle = open( accessSecretFile, "r" )
    except IOError:
        print "Could not open access token files"
        exit(3)

    try:
        consumerKey = cKeyHandle.read().strip()
        consumerSecret = cSecretHandle.read().strip()
        accessKey = aKeyHandle.read().strip()
        accessSecret = aSecretHandle.read().strip()
    except IOError:
        print "Could not read in keys/secrets"
        exit(2)

    cKeyHandle.close()
    cSecretHandle.close()
    aKeyHandle.close()
    aSecretHandle.close()

    auth = tweepy.OAuthHandler( consumerKey, consumerSecret )
    auth.set_access_token( accessKey, accessSecret )

    api = tweepy.API(auth)

    return api

def main( args ):
    addr = ''
    port = 8888
    keyFileName = ''
    secretFileName = ''
    accessTokenFile = ''
    accessSecretFile = ''
    try:
        opts, args = getopt.getopt( args, 'i:p:k:s:t:f:' )
    except getopt.GetoptError:
        exit(-1)
    for opt, arg in opts:
        if opt == '-k':
            keyFileName = arg
        elif opt == '-s':
            secretFileName = arg
        elif opt == '-t':
            accessTokenFile = arg
        elif opt == '-f':
            accessSecretFile = arg

    try: 
        port = int(port)
    except ValueError:
        print "Please supply an integer value for the port number"
        exit(4)
    birdy = authenticate( keyFileName, secretFileName, accessTokenFile, accessSecretFile )
    listener = StdOutListener(birdy)
    stream = tweepy.Stream(auth = birdy.auth, listener=listener)
    stream.userstream()
    

if( __name__ == '__main__' ):
    main( sys.argv[1:] )

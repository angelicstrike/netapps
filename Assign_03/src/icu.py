from pyzipcode import ZipCodeDatabase
import math
import code
import ephem
import argparse
import requests
import json
import smtplib
import pygame 
import datetime
import RPi.GPIO as GPIO
import time
import threading
import signal
import sys

GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)

nextViewing = None

smtpUser = 'team14netapps@gmail.com'
smtpPass = 'netapps14'
toAdd = '5712307811@txt.att.net'
fromAdd = smtpUser
subject = 'Satellite Viewing'
header = 'To: ' + toAdd + '\n' + 'From: ' + fromAdd + '\n' + 'Subject: ' + subject
body = ''
#ISS (Zarya) NOARD CAT ID is 25544
SPACE_TRACK_QUERY_URL = 'https://www.space-track.org/basicspacedata/query/class/tle_latest/NORAD_CAT_ID/%s/orderby/EPOCH%%20asc/metadata/false/'
NOAA_BASE_URL   = 'http://forecast.weather.gov/MapClick.php?lat=%f&lon=%f&FcstType=json'
sendmsg = True
e = threading.Event()
def flashy_sound( e ):
    global sendmsg, header, fromAdd, toAdd
    
    while True:
        seconds = 1000
        if (nextViewing is not None):
            tdelt = nextViewing - datetime.datetime.now()
            seconds = tdelt.total_seconds()
# and seconds <= 900 and seconds >= 300
        if (e.isSet() and seconds <= 900 and seconds >= 300):
            if (sendmsg == True):
                s = smtplib.SMTP('smtp.gmail.com', 587)
                s.ehlo()
                s.starttls()
                s.ehlo()
                s.login(smtpUser, smtpPass) 
                s.sendmail(fromAdd, toAdd, header + '\n\n' + body)
                sendmsg = False
                s.quit()

            GPIO.output(12, GPIO.HIGH)
            time.sleep(1)
            pygame.mixer.music.play()
            while pygame.mixer.music.get_busy() == True:
                continue
            GPIO.output(12, GPIO.LOW)
            time.sleep(1)
t = threading.Thread(name='flashy_blinky_sound', target=flashy_sound, args=(e,))
#NOAA responses are all in json
def fetch_noaa_data( zipcode ):
    global NOAA_BASE_URL
    try:
        z = int( zipcode, 10 )
    except:
        print "Please give a numerical zipcode"
        exit(3)

    zcdb = ZipCodeDatabase()
    try:
        area = zcdb[z]
    except:
        print "Could not find zipcode in database"
        exit(4)

    r = requests.get( NOAA_BASE_URL % (area.latitude, area.longitude) )
    observe = json.loads(r.text.replace('\t', '').replace('\n',''))

#also returns the utc offset
    return observe, area.timezone

def get_identity():
    try:
        f = open( "id.key", "r" )
    except IOError:
        print "Could not open ID file"
        exit(5)
    user_name = f.readline().strip()
    password = f.readline().strip()

    f.close()

    return user_name, password

#http://space.stackexchange.com/questions/4339/calculating-which-satellite-passes-are-visible
def datetime_from_str( t ):
    year, month, day, hour, minute, second = t.tuple()
    dt = datetime.datetime(year, month, day, hour, minute, int(second) )
    return dt

def within_hour( sat, sun, isSunrise ):
    delta = sun - sat 
    if not isSunrise:
# if it's negative and within an hour (3600 seconds), that means the satellite rose before the sun
        if 0 > delta.total_seconds() >= -3600:
            return True
        else: 
            return False
    else:
        if 0 <= delta.total_seconds() < 3600:
            return True
        else:
            return False

def radians_to_degrees( rads ):
    return rads * (180/math.pi)

def get_ephemeris( dt, offset, lat, lon, elev, tle0, tle1, tle2 ):
    results = { 'WillRise' : False, 'Sunlight' : False, 'WithinHour' : False}
    observer = ephem.Observer()
    sun = ephem.Sun()
    satellite = ephem.readtle( tle0, tle1, tle2 )

    observer.lat = lat
    observer.lon = lon
    observer.elevation = int(elev)
#get the next pass from today
    observer.date = dt
    observer.horizon = '25:0' # if it's good enough for the naval research institute, it's good enough for me
    try:
        pass_info = observer.next_pass( satellite )
    except ValueError:
        return results, None

    #if we get this far, that means that satellite will at least pass over us
    #whether or not it's at the right time is what we're calculating below
    satellite.compute( observer )
    results['WillRise'] = True
    rise_date = datetime_from_str( pass_info[0] )
    observer.date = rise_date
#this is done to get the risings and settings nearest to when the satellite passes overhead
#rise and setting is determined by when the upper limb of the body touches the horizon, so rise
#isn't quite sufficient for visibility. So go with max time instead?
    observer.horizon = '0'

    sun.compute( observer )
    delta = datetime.timedelta(days=0, hours=offset)

#BECAUSE FOR SOME STUPID REASON, THESE ARE IN UTC, BUT NOTHING ELSE IS
    prevRise = datetime_from_str( observer.previous_rising(sun) ) + delta
    nextRise = datetime_from_str( observer.next_rising(sun) ) + delta
    prevFall = datetime_from_str( observer.previous_setting(sun) ) + delta
    nextFall = datetime_from_str( observer.next_setting(sun) ) + delta
    duration = pass_info[4].datetime() - pass_info[0].datetime() 
 
    #results['Sunlight'] = -10 > radians_to_degrees(sun.alt) > -25
    results['Sunlight'] = True
    results['WithinHour'] = within_hour( rise_date, nextRise, True) | within_hour( rise_date, prevRise, True) 
    results['WithinHour'] = results['WithinHour'] | within_hour( rise_date, nextFall, False) | within_hour( rise_date, prevFall, False)
#next_rising and next_setting

    return results, pass_info[0], pass_info[4], pass_info[1], pass_info[5]

def get_tle_info( satellite_name ):
    global SPACE_TRACK_QUERY_URL
    username, password = get_identity()
    data = 'identity=' + username + '&password=' + password + '&query=' + (SPACE_TRACK_QUERY_URL % satellite_name)
#no, i don't know why this has to be here, but if it's not, space-track gives you "undefined index:password"
#which is so stupid and vague.
    headers = { 'Content-Type' : 'application/x-www-form-urlencoded' }
    r = requests.post( 'https://www.space-track.org/ajaxauth/login', data=data, headers=headers )
    j = r.json()

#print out tle info
    for key, value in j[0].items():
        print key + ": " + value

    if type(j) is list and len(j) > 0:
        return j[0]
    elif type(j) is dict:
        return j
    else:
        print "No TLE data received"
        exit(5)

def start_requests( zipcode, satellite_name ):
    tle = get_tle_info( satellite_name )
    print ""
    weather, offset = fetch_noaa_data( zipcode )

    obs = weather['currentobservation']
    start_date = datetime.datetime.utcnow()
    one_day = datetime.timedelta(days=1)
    tmp = start_date

    viewable_events = set()
    st_data = set()
    af_data = set()
    at_data = set()
#print out weather data
    print "Forcast for the next 15 days"
    print "============================"
    for w in weather['data']['weather']:
        print str(tmp) + ": " + w
        tmp = tmp + one_day

    tmp = start_date
    for w in weather['data']['weather']:
        if 'Clear' in w:
            v, rt, st, af, at = get_ephemeris( tmp, offset, obs['latitude'], obs['longitude'], obs['elev'], str(tle['TLE_LINE0']), str(tle['TLE_LINE1']), str(tle['TLE_LINE2']) )
            isViewable = reduce( lambda acc,value: acc and value, v.itervalues(), True )
            if isViewable and rt != None:
                viewable_events.add( rt )
                st_data.add( st )
                af_data.add( af )
                at_data.add( at )
				
        tmp = tmp + one_day
    print "\nNext Viewable Events"
    print "---------------------"
    if (len(viewable_events) > 0) :
        for i in range(0,len(viewable_events)):
            print list(viewable_events)[i]
            duration = list(st_data)[i].datetime() - list(viewable_events)[i].datetime() 
            print "Rise time: " + str(list(viewable_events)[i].datetime()) + "\n Set time: " + str(list(st_data)[i].datetime()) + "\n Duration: " + str(duration) + "\nStart from: " + str(list( af_data)[i]) + "\nGoing to: " + str(list(at_data)[i])
        start_waiting(viewable_events, st_data, af_data, at_data)
    else : 
        print "No viewable events in the near future"

def start_waiting(viewable, data_settime, data_azfrom, data_azto):
    global nextViewing
    global sendmsg
    global body
    e.set()
    t.start()
    while True:
        now = datetime.datetime.now()
        next = (list(viewable)[0]).datetime()
        duration = list(data_settime)[0].datetime() - list(viewable)[0].datetime() 
        body = " Rise time: " + str(list(viewable)[0].datetime()) + "\n  Set time: " + str(list(data_settime)[0].datetime()) + "\n  Duration: " + str(duration) + "\nStart from: " + str(list(data_azfrom)[0]) + "\n  Going to: " + str(list(data_azto)[0])
        tdelt = next - now;
        secs = tdelt.total_seconds()
        if (secs <= 0):
            viewable.pop()
            data_settime.pop()
            data_azfrom.pop()
            data_azto.pop()
            next = list(viewable)[0]
            sendmsg = True			
        nextViewing = next;
#somehow put this into a scheduling thing?

def signal_handler(signal, frame): 
    GPIO.cleanup()
    t.join()
    sys.exit(0)
	
def main():
    parser = argparse.ArgumentParser( description='Satelite visibility' )
    parser.add_argument( '--zipcode', '-z', help='zipcode of viewing area', nargs=1 )
    parser.add_argument( '--satellite', '-s', help='name of satellite', nargs=1 )
    signal.signal(signal.SIGINT, signal_handler)
    pygame.mixer.init()
    pygame.mixer.music.load("sound.wav")
    args = parser.parse_args()
    start_requests( args.zipcode[0], args.satellite[0] )

if __name__ == '__main__':
    main()
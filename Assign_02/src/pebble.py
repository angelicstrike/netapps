import json
import pika
import code
import uuid
import time
import socket
import shelve
import getpass
import argparse

from time import sleep
from zeroconf import ServiceBrowser, Zeroconf, ServiceStateChange

shelf = shelve.open("pebble_shelf.db", writeback=True)
response = ""

def recv_msg( channel, method, properties, msg_body ):
#multiple responses have to come in as a list
    global response
    resp = json.loads( msg_body )
    if type(resp) is list:
        for r in resp:
            print r
            if( 'MsgID' in r ):
                shelf[ str(r['MsgID']) ] = resp
            else:
                shelf["msg" + str(time.mktime(time.localtime()))] = resp
    else:
        print resp
        if( 'MsgID' in resp ):
            shelf[ str(resp['MsgID']) ] = resp
        else:
            shelf["msg" + str(time.mktime(time.localtime()))] = resp
    channel.stop_consuming()
    response = resp

#The only reason I have this here is because zerconf doesn't allow you to pass any kind of 
#extra parameter to it's handler functions. I miss C. 
class ZC( Zeroconf ):
    def __init__( self, act, sub, msg, Qm, Qs, Qa, QA ):
        super(ZC,self).__init__()
        self.act = act
        self.sub = sub
        self.msg = msg
        
        self.Qm = Qm
        self.Qs = Qs
        self.Qa = Qa
        self.QA = QA

    def pull( self ):
        request = { "Action": self.act }
        if( self.QA != None ):
            request['Author'] = self.QA[0]
        if( self.Qa != None ):
            request['Age'] = self.Qa[0]
        if( self.Qm != None ):
            request['Message'] = self.Qm[0]
        if( self.Qs != None ):
            request['Subject'] = self.Qs[0]
        return request

    def push( self ):
        if( self.sub == None ):
            print "Invalid subject"
            exit(3)
        if( self.msg == None ):
            print "Invalid message"
            exit(4)

        request = { "Action":"push" }
        request['Author'] = getpass.getuser()
        request['MsgID'] = "msg" + str(time.mktime(time.localtime()))
        request['Age'] =  9001
        request['Subject'] = self.sub[0]
        request['Message'] = self.msg[0]
        return request

    def do_action( self, ch, name ):
        req = {}
        if( self.act == 'push' ):
            req = self.push()
        elif( self.act == 'pullr' or self.act == 'pull' ):
            req = self.pull()
        else:
            print "Invalid command"
            exit(2)

        self.corr_id = str(uuid.uuid4())
        self.callback_queue = ch.queue_declare(exclusive=True, queue="CallbackQueue")

        ch.basic_consume( recv_msg, queue=self.callback_queue.method.queue, no_ack=True )
        ch.basic_publish( exchange='', routing_key=name, body=json.dumps(req), properties=pika.BasicProperties(reply_to=self.callback_queue.method.queue, correlation_id=self.corr_id))

        ch.start_consuming()

#store off the request we make
#send it via the msg queues
#wait until we get a response
#store the resonse
#die
        
#Gets called anything a service changes state. Conditional makes it so we don't act on a removal of
#a service (since we don't care about that state and there's only two states, addition and removal)
def on_service_state_change( zeroconf, service_type, name, state_change ):
    global shelf
    if state_change is ServiceStateChange.Added:
        info = zeroconf.get_service_info( service_type, name )
        print("  Name: %s" % name )
        if info:
            addr = socket.inet_ntoa(info.address)
            port = info.port
            queue = ""
            for c in info.text:
                if ord(c) > 32:
                    queue = queue + str(c)
            print("  Address: %s:%d" % (addr, port))
            print("  Weight: %d, priority: %d" % (info.weight, info.priority))
            print("  Server: %s" % (info.server))
            print("  Queue: %s" % queue)
            #msg_broker = pika.BlockingConnection( pika.ConnectionParameters(host=info.server, port=port, credentials=pika.PlainCredentials('team14', 'password') ))
            msg_broker = pika.BlockingConnection( pika.ConnectionParameters(host=info.server, port=port, credentials=pika.PlainCredentials('guest', 'guest') ))
            ch = msg_broker.channel()

            zeroconf.do_action( ch, queue )

            try:
                msg_broker.close()
                shelf.close()
                zeroconf.close() 
            except Exception:
                exit(1)

def main( ):
    # Argument parsing
    global response
    parser = argparse.ArgumentParser(description='Push and Pull messages from a bottle')
    parser.add_argument( '--action', '-a', help='action to take', nargs=1)
    parser.add_argument('--subject', '-s', help='subject of message', nargs=1)
    parser.add_argument('--message', '-m', help='message body', nargs=1)

    parser.add_argument('-Qm', help='Query by message body', nargs=1)
    parser.add_argument('-Qs', help='Query by subject', nargs=1)
    parser.add_argument('-Qa', help='Query by age', nargs=1)
    parser.add_argument('-QA', help='Query by author', nargs=1)
    
    args = parser.parse_args()

    zc = ZC( args.action[0], args.subject, args.message, args.Qm, args.Qs, args.Qa, args.QA )
    browser = ServiceBrowser( zc, "_http._tcp.local.", handlers=[on_service_state_change])

    while response == '':
        sleep(0.1)

if( __name__ == '__main__' ):
    main()

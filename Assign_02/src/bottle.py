#!/usr/bin/env python
import pika
import uuid
import avahi
import dbus
import sys
import shelve
import signal
import json
import code
import re
#import RPi.GPIO as GPIO
import time

#Set up script to use the right pin configuration
#GPIO.setmode(GPIO.BOARD)
#GPIO.setwarnings(False)

#LED code taken from
#http://www.jamesbmarshall.com/2015/04/python-4-bit-led-counter-on-raspberry-pi-2/
#Define some variables
LED_counter = 0                     #The current number in decimal
bit_counter = 0                     #The current bit
current_num = ""                   #The current number in binary
LED_array = [[11,0],[12,0],[13,0],[15,0]]        #The LED configuration array
Pin_array = [11,12,13,15]

#Set all the pins to outputs
#for index in range(len(Pin_array)):
#    GPIO.setup(Pin_array[index], GPIO.OUT)
#
###Reset all the pins to 0
#def resetPins():
#    for index in range(len(Pin_array)):
#        GPIO.output(Pin_array[index], 0)
#
###Turn the LEDs on or off
#def ledSwitch(theLEDs, *args):
#    LED_counter = 0
#
#    while LED_counter < 4:
#        GPIO.output(theLEDs[LED_counter][0], theLEDs[LED_counter][1])
#        LED_counter += 1

class bottle(object):
    def __init__(self, name, port, stype="_http._tcp", domain="", h="", text="Team14Queue"):
	global bit_counter
	
        #avahi setup
        self.shelf = shelve.open('bottle_shelf.db', writeback=True)
        print "Initial Shelf:"
 	print(self.shelf)
        print ""
        self.name = name
        self.stype = stype
        self.domain = domain
        self.host = h
        self.port = port
        self.text = text
	#resetPins()
	lednum = bin(len(self.shelf))[2:].zfill(4)
	while bit_counter < 4:
           LED_array[bit_counter][1] = int(lednum[bit_counter])
	   bit_counter += 1
	bit_counter = 0
	#GPIO.output(Pin_array[3], 1)
	#ledSwitch(LED_array)
        #rabbit mq setup
        self.connection = pika.BlockingConnection(pika.ConnectionParameters())
        self.channel = self.connection.channel()
        result = self.channel.queue_declare(exclusive=False, queue=self.text)
        
        self.callback_queue = result.method.queue

        self.channel.basic_consume(self.on_response, no_ack=True, queue=self.callback_queue)

    def search_shelf( self, regex, k ):
        results = []
        for key, value in self.shelf.iteritems():
            if( re.search( regex, str(value[k]) ) != None ):
                results.append( json.dumps(value) )
        return results

    def get_matches( self, jdata ):
        sets = []
        if( 'Age' in jdata ):
            age = int(re.search(r'\d+', jdata['Age']).group())
            sets.append( self.search_shelf( str(age), "Age" ) )
        if( 'Subject' in jdata ): 
            sets.append( self.search_shelf( str(jdata['Subject']), "Subject" ) )
        if( 'Message' in jdata ):
            sets.append( self.search_shelf( str(jdata['Message']), "Message" ) )
        if( 'Author' in jdata ):
            sets.append( self.search_shelf( str(jdata['Author']), "Author" ) )

        matches = list()
        if( len(sets) > 0 ):
            matches = sets[0]
        if( len(sets) > 1 ): 
            for x in sets[1:]:
                matches = set(matches) & set(x)
        return matches
            
								   
    def on_response(self, ch, method, properties, body):
        global bit_counter
	jdata = json.loads(body)
        print "Received: "
        print body
        print ""
	self.reponse = jdata
	if jdata['Action'] == "push":
            self.shelf[str(jdata['MsgID'])] = jdata;
            self.shelf.sync()
            self.channel.basic_publish(exchange='', routing_key=properties.reply_to, body=json.dumps({"Status":"Success"}), properties=pika.BasicProperties(correlation_id=properties.correlation_id))
            print "Successful push"
            print ""
	if jdata['Action'] == "pull":
#get number value in age
            matches = self.get_matches( jdata )

            msg = []
            for x in matches:
                msg.append( json.loads(x) )
            print "Sending:"
            print json.dumps(msg)
            print ""
            self.channel.basic_publish(exchange='', routing_key=properties.reply_to, body=json.dumps(msg))
	if jdata['Action'] == "pullr":
            matches = self.get_matches( jdata )
            msg = []
            for x in matches:
                d = json.loads(x)
                del self.shelf[str(d['MsgID'])]
                msg.append(d)
            print "Sending:"
            print json.dumps(msg)
            print ""
            self.channel.basic_publish(exchange='', routing_key=properties.reply_to, body=json.dumps(msg))
			
	lednum = bin(len(self.shelf))[2:].zfill(4)
	while bit_counter < 4:
           LED_array[bit_counter][1] = int(lednum[bit_counter])
	   bit_counter += 1
	bit_counter = 0
	#GPIO.output(Pin_array[3], 1)
	#ledSwitch(LED_array)

    def publish(self):
        bus = dbus.SystemBus()
        server = dbus.Interface(bus.get_object( avahi.DBUS_NAME,avahi.DBUS_PATH_SERVER),avahi.DBUS_INTERFACE_SERVER)

        g = dbus.Interface( bus.get_object(avahi.DBUS_NAME, server.EntryGroupNew()), avahi.DBUS_INTERFACE_ENTRY_GROUP)

        g.AddService(avahi.IF_UNSPEC, avahi.PROTO_UNSPEC,dbus.UInt32(0),self.name, self.stype, self.domain, self.host, dbus.UInt16(self.port), self.text)

        g.Commit()
        self.group = g
	self.channel.start_consuming()
    def unpublish(self):
        self.group.Reset()
    def sig_handler( self, signal, frame ):
        self.shelf.sync()
        #GPIO.cleanup()    
        sys.exit(0)

	
if __name__ == "__main__":
    b = bottle(name = "BottleService", port=5672)
    signal.signal( signal.SIGINT, b.sig_handler )
    b.publish()
